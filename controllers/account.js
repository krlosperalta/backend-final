'use strict'

var requestJson = require('request-json');
const config=require('../config');
const url=config.mlab_host+config.mlab_db+'collections/';


//GET ALL ACCOUNTS
function getAccounts(request,response){
    console.log("Get todas las cuentas");
    var client = requestJson.createClient(url);
    const queryName='q={"cliente":'+request.params.id+'}&';
    const filter='f={"_id":0}&';
    client.get(config.mlab_collection_account+'?'+queryName + filter +config.mlab_key, function(err, res, body) {
      response.send(body);
    });
}

//POST ACCOUNT
function saveAccount(request,response){
    console.log("Creacion de Cuentas");
    var client = requestJson.createClient(url);
    console.log(request.body.idCliente);
    var data = {
      "idCuenta" : Math.floor(1e9 + (Math.random() * 9e9)),
      "cliente" : request.body.idCliente,
      "movimiento" : [],
      "saldo" : 0.00
    };
    client.post(config.mlab_collection_account+'?'+config.mlab_key, data, function(err, res, body) {
      if(err)
        console.log(err);
      response.send(body);
    });
};

module.exports={
  getAccounts,
  saveAccount
};
